require 'govern'

Dir[File.join(File.dirname(__FILE__),"support/**/*.rb")].each { |f| require f }

RSpec.configure do |config|
  config.run_all_when_everything_filtered = true
  config.filter_run :focus
  config.order = 'random'

  config.before(:each) do
    Govern.reset
    PubSub.add_subscription(Govern::Governor.new)
  end
  config.after(:each) do
    PubSub.reset
    Govern.reset
  end
end
