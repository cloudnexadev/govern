require 'spec_helper'

describe Govern do

  describe 'Configurable' do

    it '.configure allows fluent configuration' do
      Govern.configure do |config|
        config.backend = :active_record
        config.validate_policies = false
      end

      expect(Govern.config.backend).to eq :active_record
      expect(Govern.config.validate_policies).to be_falsey
    end

  end

  describe 'BackendAware' do

    it '.backend gets a class of configured backend' do
      expect(Govern.backend).to eq(Govern::Backend::InMemory::Policy)
    end

  end

  describe 'Resettable' do

    it '.reset makes all configs values default' do
      Govern.configure do |config|
        config.backend = :dummy
        config.validate_policies = false
      end
      Govern.reset
      expect(Govern.config.backend).to eq :in_memory
      expect(Govern.config.validate_policies).to be_truthy
    end

  end

end