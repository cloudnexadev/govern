require 'spec_helper'
require 'json'

module Govern::Backend::Dummy
  class Policy
    include Govern::Policy
    attr_accessor :model, :events, :workflow, :workflow_input_params
  end
end

describe Govern::Policy do

  let!(:publisher) do
    Class.new do
      include Govern::Governable
      governed_as_actor 'gov:publisher'
    end
  end

  let!(:workflow) do
    Class.new do
      include Govern::Governable
      governed_as_activity 'gov:workflow' do
        input_param :p_int, :integer
        input_param :p_str, :string
      end
    end
  end

  let(:policy_class) { Govern::Backend::Dummy::Policy }

  describe 'InstanceMethods' do

    let(:policy) do
      policy = policy_class.new
      policy.model = 'gov:publisher:id'
      policy.events = %w(published)
      policy.workflow = 'gov:workflow'
      policy.workflow_input_params = {:p_int => 1, :p_str =>  'abc'}
      policy
    end

    describe '.invoke' do

      it 'raises error when activity is not registered' do
        policy.workflow = 'test'
        expect { policy.invoke }.to raise_error('activity is not registered.')
      end

      it "calls activity's execute method with mapped input params" do
        expect_any_instance_of(workflow).to receive(:execute).with({:p_int => 1, :p_str =>  'abc'})
        policy.invoke(nil, nil, nil, false, false)
      end

      it 'raises error when invalid params' do
        policy.workflow_input_params = {:p_int => 'aaa', :p_str =>  'abc'}
        expect { policy.invoke }.to raise_error("activity input param mapping error: 'aaa' is not an integer.")
      end

    end

    describe '.model_name' do

      it 'raises error when no : in model' do
        policy.model = 'test'
        expect { policy.model_name }.to raise_error('invalid policy model.')
      end

      it 'returns left part before last colon' do
        policy.model = 'test:id'
        expect(policy.model_name).to eq 'test'
      end

    end

    describe '.model_id' do

      it 'raises error when no : in model' do
        policy.model = 'test'
        expect { policy.model_id }.to raise_error('invalid policy model.')
      end

      it 'returns right part after last colon if not *' do
        policy.model = 'test:id'
        expect(policy.model_id).to eq 'id'
      end

      it 'returns nil if right part after last colon is *' do
        policy.model = 'test:*'
        expect(policy.model_id).to be_nil
      end

    end

    describe '.to_h' do

      it 'builds a hash' do
        expect(policy.to_h).to eq({:model => 'gov:publisher:id', :events => %w(published), :workflow => 'gov:workflow', :workflow_input_params => {:p_int => 1, :p_str =>  'abc'}})
      end

      it 'does not include workflow_input_params key if it is empty' do
        policy.workflow_input_params = []
        expect(policy.to_h).to eq({:model => 'gov:publisher:id', :events => %w(published), :workflow => 'gov:workflow'})
      end

    end

    it '.to_json builds a json string' do
      expect(policy.to_json).to eq({:model => 'gov:publisher:id', :events => %w(published), :workflow => 'gov:workflow', :workflow_input_params => {:p_int => 1, :p_str =>  'abc'}}.to_json)
    end

  end

  describe 'ClassMethods' do

    describe '.from_json' do

      before do
        Govern.config.backend = :dummy
      end

      it 'parses valid policy json' do
        policy = policy_class.from_json '{"model": "gov:publisher:*", "events": ["all"], "workflow": "gov:workflow"}'
        expect(policy.model).to eq('gov:publisher:*')
        expect(policy.events).to eq(['all'])
        expect(policy.workflow).to eq('gov:workflow')
      end

      it 'throws error if invalid json' do
        expect { policy_class.from_json 'abc' }.to raise_error 'error parsing policy from json.'
      end

      it 'throws error if actor is not registered' do
        expect { policy_class.from_json '{"model": "publisher:*", "events": ["all"], "workflow": "gov:workflow"}' }.to raise_error 'actor is not registered.'
      end

      it 'throws error if activity is not registered' do
        expect { policy_class.from_json '{"model": "gov:publisher:*", "events": ["all"], "workflow": "workflow"}' }.to raise_error 'activity is not registered.'
      end

    end

  end

end
