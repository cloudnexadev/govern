require 'spec_helper'

describe Govern::Workflows::InputParamEvaluator do

  let(:param) { Govern::Workflows::InputParam.new(name: 'param') }
  let(:param_with_default) { Govern::Workflows::InputParam.new(name: 'param_with_default', default: :default) }
  let(:param_required) { Govern::Workflows::InputParam.new(name: 'param_required', required: true) }
  let(:param_not_allowing_blank) { Govern::Workflows::InputParam.new(name: 'param_not_allowing_blank', allow_blank: false) }

  let(:klass) { Govern::Workflows::InputParamEvaluator }

  describe '.initialize' do

    describe 'event' do

      it 'accepts nil, string or symbol as event' do
        [:fired, 'fired', nil].each do |evt|
          expect { klass.new(param, nil, evt) }.not_to raise_error
        end
      end

      it 'raises error if event is not a string, symbol or nil' do
        expect { klass.new(param, nil, []) }.to raise_error('invalid event param.')
      end

      it 'wraps event in open struct with name property' do
        [:fired, 'fired'].each do |evt|
          expect(klass.new(param, nil, evt).event.name).to eq evt
        end
      end

      it 'wraps event with <undefined> name if nil' do
        expect(klass.new(param, nil, nil).event.name).to eq '<undefined>'
      end

    end

    describe 'payload' do

      it 'accepts nil or Hash' do
        [nil, {:p => :v}].each do |payload|
          expect { klass.new(param, nil, nil, nil, payload) }.not_to raise_error
        end
      end

      it 'raises error if payload is not nil and not a Hash' do
          expect { klass.new(param, nil, nil, nil, []) }.to raise_error('invalid payload param.')
      end

      it 'wraps payload in open struct' do
          expect(klass.new(param, nil, nil, nil, {:p => :v}).payload.p).to eq :v
      end

    end

  end

  describe '.eval_expressions' do

    it 'accepts properties for model, item, publisher, event, payload, args' do
      %w(model item publisher event payload args).each do |m|
        val = "{{#{m}.id}}"
        expect(klass.new(param, val).eval_expressions).to eq ["#{m}.id"]
      end
    end

    it 'other expressions are not evaluated' do
      expect(klass.new(param, '{{bla}}').eval_expressions).to eq []
    end

    it 'finds all expressions to evaluate' do
      expect(klass.new(param, '{{model.id}}-{{event.name}}').eval_expressions).to eq %w(model.id event.name)
    end

  end

  describe '.value' do

    it 'calls .value on input param instance to make conversions' do
      expect(param).to receive(:value).with('123').and_return(123)
      expect(klass.new(param, '123').value).to eq 123
    end

    context 'without mapping expressions' do

      it 'returns constant mapped value' do
        expect(klass.new(param, 1).value).to eq 1
        expect(klass.new(param, 'abc').value).to eq 'abc'
      end

      it 'returns default if no mapped value' do
        expect(klass.new(param_with_default, nil).value).to eq :default
      end

      it 'raises error when no default, required and no mapped value' do
        [nil, '', ' '].each do |val|
          expect { klass.new(param_required, nil).value }.to raise_error("#{param_required.name} input param required.")
        end
      end

    end

    context 'with mapping expressions' do

      let(:model) { double('model') }
      let(:event) { 'fired' }
      let(:payload) { {:prop => :prop_value} }

      it 'evaluates model properties' do
        expect(model).to receive(:id).and_return(1)
        expect(klass.new(param, '{{model.id}}', event, model).value).to eq '1'
      end

      it 'evaluates item and publisher and aliases to model' do
        expect(model).to receive(:id).twice.and_return(1)
        expect(klass.new(param, '{{item.id}}-{{publisher.id}}', event, model).value).to eq '1-1'
      end

      it 'evaluates event properties' do
        expect(klass.new(param, '{{event.name}}', event).value).to eq event
      end

      it 'evaluates payload properties' do
        expect(klass.new(param, '{{payload.prop}}', nil, nil, payload).value).to eq 'prop_value'
      end

      it 'evaluates args as alias to payload' do
        expect(klass.new(param, '{{args.prop}}', nil, nil, payload).value).to eq 'prop_value'
      end
    end

  end

end