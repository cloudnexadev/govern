require 'active_support'
require 'pub_sub'

require 'govern/version'

require 'govern/configs/event'
require 'govern/configs/actor'
require 'govern/configs/activity'
require 'govern/config'

require 'govern/configurable'
require 'govern/backend_aware'
require 'govern/resettable'
require 'govern/finders'
require 'govern/policy'

require 'govern/backend/in_memory/policy'
require 'govern/governor'

require 'govern/governable'

require 'govern/workflows/input_param'
require 'govern/workflows/input_param_evaluator'

require 'govern/workflows/input_params/integer'
require 'govern/workflows/input_params/string'


module Govern
  include Configurable
  include BackendAware
  include Resettable
  include Finders
end
