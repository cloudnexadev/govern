module Govern
  module Workflows
    module InputParams
      class String < InputParam

        def value(val)
          val
        end

      end
    end
  end
end