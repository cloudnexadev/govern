module Govern
  module Workflows
    module InputParams
      class Integer < InputParam

        def value(val)
          Integer(val)
        rescue ArgumentError, TypeError
          raise "'#{val}' is not an integer."
        end

      end
    end
  end
end