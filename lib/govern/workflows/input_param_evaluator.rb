module Govern
  module Workflows
    class InputParamEvaluator

      attr_reader :event, :model, :payload, :input_param, :mapped_value, :skip_params_evaluation
      attr_reader :error

      alias :item :model
      alias :publisher :model

      alias :args :payload

      EVAL_PATTERN = /\{\{((model|item|publisher|event|payload|args)\.[a-z_?]+)\}\}/

      def initialize(input_param, mapped_value, event = nil, model = nil, payload = nil, skip_params_evaluation = false)
        @model, @input_param, @mapped_value, @skip_params_evaluation = model, input_param, mapped_value, skip_params_evaluation

        raise 'invalid event param.' if !event.nil? && !event.is_a?(String) && !event.is_a?(Symbol)
        @event = OpenStruct.new(name: (event.nil? ? '<undefined>' : event))

        raise 'invalid payload param.' if !payload.nil? && !payload.is_a?(Hash)
        @payload = OpenStruct.new(payload.is_a?(Hash) ? payload : {})

        @eval_expressions = nil
        @error = nil
      end

      def valid?
        @error = nil
        value
        true
      rescue => e
        @error = e.message
        false
      end

      def value
        val = mapped_value

        unless skip_params_evaluation
          eval_expressions.each do |e|
            replace_val = instance_eval(e)
            val = val.gsub("{{#{e}}}", replace_val.to_s)
          end
        end

        val = val.blank? ? @input_param.default : val
        raise "#{input_param.name} input param required." if input_param.required? && val.nil?

        input_param.value(val)
      end

      def eval_expressions
        if @eval_expressions.nil?
          @eval_expressions = []
          if !mapped_value.nil? && mapped_value.is_a?(String)
            mapped_value.scan(EVAL_PATTERN).each do |m|
              @eval_expressions << m[0]
            end
          end
        end
        @eval_expressions
      end

      private



    end
  end
end