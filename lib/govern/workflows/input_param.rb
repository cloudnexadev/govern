module Govern
  module Workflows
    class InputParam

      attr_reader :name, :default, :required

      def initialize(options = {})
        @name = options[:name]
        @default = options[:default]

        @required = options[:required]
        @required = false if @required.nil?
      end

      def value(val)
        val
      end

      def required?
        required
      end

    end
  end
end