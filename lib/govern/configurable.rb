module Govern
  module Configurable
    extend ActiveSupport::Concern

    module ClassMethods

      def config
        @config ||= Govern::Config.new
      end

      def configure(&block)
        yield(config) if block_given?
      end
    end

  end
end