module Govern
  class Governor

    def on_event(event, publisher, payload = {})
      raise "Govern backend doesn't respond to :find_policies." unless Govern.backend.respond_to?(:find_policies)
      policies = Govern.backend.find_policies(event, publisher, payload)
      policies.each do |policy|
        policy.invoke(event, publisher, payload)
      end
    end

  end
end