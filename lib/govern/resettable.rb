module Govern
  module Resettable
    extend ActiveSupport::Concern

    module ClassMethods

      def reset
        @config = Govern::Config.new
      end
    end

  end
end