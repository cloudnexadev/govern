module Govern
  class Config

    attr_accessor :validate_policies, :backend
    attr_reader :input_param_types, :actors, :activities

    def initialize
      @input_param_types = {
          :string => 'Govern::Workflows::InputParams::String',
          :integer => 'Govern::Workflows::InputParams::Integer'
      }
      @actors = {}
      @activities = {}

      @validate_policies = true
      @backend = :in_memory
    end

    def register_param_type(name, klass)
      input_param_types[name] = klass
    end

    def register_actor(name, klass, &block)
      @actors_map = nil
      actors[name] = Configs::Actor.new(klass, &block)
    end

    def register_activity(name, klass, &block)
      @activities_map = nil
      activities[name] = Configs::Activity.new(klass, &block)
    end

    def validate_policies?
      validate_policies
    end

    def backend_class
      if @backend_klass.nil?
        if backend.is_a? Symbol
          @backend_klass = "Govern::Backend::#{backend.to_s.classify}::Policy".constantize
        end
        if backend.is_a? Class
          @backend_klass = backend
        end
        if backend.is_a? String
          @backend_klass = backend.constantize
        end
      end
      @backend_klass
    end

    def actors_class_name_map
      if @actors_map.nil?
        @actors_map = {}
        actors.each do |name, config|
          @actors_map[config.klass_name] = name
        end
      end
      @actors_map
    end

    def activities_class_name_map
      if @activities_map.nil?
        @activities_map = {}
        activities.each do |name, config|
          @activities_map[config.klass_name] = name
        end
      end
      @activities_map
    end

  end
end