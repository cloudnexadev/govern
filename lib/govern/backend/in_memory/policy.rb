module Govern
  module Backend
    module InMemory
      class Policy
        include Govern::Policy

        attr_accessor :model, :events, :workflow, :workflow_input_params

        def initialize
        end

        class << self

          def find_by_event_and_model(event, model)
            @policies.select{|p| p.model == model && p.events.include?(event)}
          end

          private

          def policies
            @policies ||= []
          end

        end

      end
    end
  end
end