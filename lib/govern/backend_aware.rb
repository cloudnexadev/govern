require 'active_support/core_ext/string'

module Govern
  module BackendAware
    extend ActiveSupport::Concern

    module ClassMethods

      def backend
        Govern.config.backend_class
      end

    end

  end
end