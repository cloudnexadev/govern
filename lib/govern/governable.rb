module Govern
  module Governable
    extend ActiveSupport::Concern

    module ClassMethods

      def governed_as_actor(name, &block)
        Govern.config.actors[name] = Configs::Actor.new(self, &block)
      end

      def governed_as_activity(name, &block)
        Govern.config.activities[name] = Configs::Activity.new(self, &block)
      end

    end

  end
end