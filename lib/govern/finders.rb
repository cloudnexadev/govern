module Govern
  module Finders
    extend ActiveSupport::Concern

    module ClassMethods

      def find_actor_by_class_name(class_name)
        Govern.config.actors_class_name_map[class_name]
      end

      def find_activity_by_class_name(class_name)
        Govern.config.activities_class_name_map[class_name]
      end

    end

  end
end