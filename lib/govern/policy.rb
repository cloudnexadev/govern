require 'active_support/hash_with_indifferent_access'

module Govern
  module Policy
    extend ActiveSupport::Concern

    included do

      # backend class should have model, events, workflow, workflow input params (values/mapping)

      def model_name
        index = self.model.nil? ? nil : self.model.rindex(':')
        raise 'invalid policy model.' if index.nil?
        self.model[0, index]
      end

      def model_class
        config = Govern.config.actors[self.model_name]
        config.nil? ? nil : config.klass
      end

      def model_id
        index = self.model.nil? ? nil : self.model.rindex(':')
        raise 'invalid policy model.' if index.nil?
        last = self.model[(index + 1)..(-1)]
        last == '*' ? nil : last
      end

      def any_model_instance?
        self.model_id.nil?
      end

      def workflow_class
        config = Govern.config.activities[self.workflow]
        config.nil? ? nil : config.klass
      end

      def valid_model?
        model_class != nil
      end

      def valid_workflow?
        workflow_class != nil
      end

      def to_h
        res = {}
        res[:model] = self.model
        res[:events] = self.events
        res[:workflow] = self.workflow
        res[:workflow_input_params] = self.workflow_input_params unless self.workflow_input_params.blank?
        res
      end

      def to_json
        self.to_h.to_json
      end

      def invoke(event = nil, model = nil, payload = nil, skip_params_evaluation = false, async = true)
        config = Govern.config.activities[self.workflow]
        raise 'activity is not registered.' if config.nil?

        self.workflow_input_params = ActiveSupport::HashWithIndifferentAccess.new(self.workflow_input_params.is_a?(Hash) ?  self.workflow_input_params : {})

        params = {}
        config.input_params.each do |p|
          pev = Govern::Workflows::InputParamEvaluator.new(p, self.workflow_input_params[p.name], event, model, payload, skip_params_evaluation)
          if pev.valid?
            params[p.name] = pev.value
          else
            raise "activity input param mapping error: #{pev.error}"
          end
        end

        if async
          if defined?(Delayed)
            config.klass.new.delay.execute params
          else
            raise 'Async job framework is not enabled. Cannot invoke policy in async mode.'
          end
        else
          config.klass.new.execute params
        end

      end

      def from_json(json)
        h = {}
        begin
          h = JSON.parse json
        rescue => e
          raise 'error parsing policy from json.'
        end

        policy = Govern.backend.new

        policy.model = h['model']
        policy.events = h['events']
        policy.workflow = h['workflow']
        policy.workflow_input_params = h['workflow_input_params']

        raise 'actor is not registered.' if policy.model_class.nil?
        raise 'activity is not registered.' if policy.workflow_class.nil?

        self.model = policy.model
        self.events = policy.events
        self.workflow = policy.workflow
        self.workflow_input_params = policy.workflow_input_params

        self
      end

    end

    module ClassMethods

      def from_json(json)
        policy = Govern.backend.new
        policy.from_json json
      end

    end

  end
end
