module Govern
  module Configs
    class Activity < Actor

      attr_reader :input_params

      def initialize(class_name, &block)
        @input_params = []
        super(class_name, &block)
      end

      def input_param(name, type, options = {})
        options[:name] = name

        type_class = Govern.config.input_param_types[type]
        type_class = type_class.constantize if type_class.is_a? String

        input_params << type_class.new(options)
      end

    end
  end
end