module Govern
  module Configs
    class Event

      attr_accessor :name

      def initialize(name, &block)
        @name = name
        instance_eval(&block)
      end

    end
  end
end