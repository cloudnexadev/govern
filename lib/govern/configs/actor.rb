module Govern
  module Configs
    class Actor

      attr_reader :class_name, :events

      def initialize(class_name, &block)
        @class_name, @events = class_name, []
        instance_eval(&block) if block_given?
      end

      def klass_name
        if class_name.is_a?(String)
          class_name
        else
          class_name.name
        end
      end

      def klass
        if class_name.is_a?(String)
          class_name.constantize
        else
          class_name
        end
      end

      def event(name, &block)
        events << Event.new(name, &block)
      end

    end
  end
end